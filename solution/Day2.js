import Solution from "./Solution.js";

export default class Day2 extends Solution {
  parseInput(input) {
    return input.split("\n").map((line) => {
      const [instruction, value] = line.split(" ");
      return { instruction, value: parseInt(value) };
    });
  }

  commands = {
    forward: (value) => (this.state.x += value),
    down: (value) => (this.state.y += value),
    up: (value) => (this.state.y -= value),
  };

  commands2 = {
    forward: (value) => {
      this.state.x += value;
      this.state.y += this.state.aim * value;
    },
    down: (value) => (this.state.aim += value),
    up: (value) => (this.state.aim -= value),
  };

  async solveSilver(input) {
    this.state = { x: 0, y: 0 };
    const comandlist = this.parseInput(input);

    comandlist.forEach(({ instruction, value }) =>
      this.commands[instruction](value)
    );

    return this.state.x * this.state.y;
  }

  async solveGold(input) {
    this.state = { x: 0, y: 0, aim: 0 };
    const comandlist = this.parseInput(input);

    comandlist.forEach(({ instruction, value }) =>
      this.commands2[instruction](value)
    );

    return this.state.x * this.state.y;
  }
}
