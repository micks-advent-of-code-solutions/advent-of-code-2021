import Solution from "./Solution.js";

const filter = (index, value) => (test) => test[index] === value;

export default class Day3 extends Solution {
  parseInput(input) {
    return input
      .split("\n")
      .map((line) => line.split("").map((char) => parseInt(char)));
  }

  findMostCommonBit(data, position) {
    const entriesCount = data.length;
    const entiresLength = data[0].length;

    let sum = 0;
    let high = 0;
    for (let entry = 0; entry < entriesCount; entry++) {
      const number = data[entry][position];
      sum += number;
      // More than half are ones already
      if (sum >= entriesCount / 2) {
        console.log("high");
        high = 1;
        break;
      }
    }
    return high;
  }

  async solveSilver(input) {
    // naive approach
    const data = this.parseInput(input);
    const entriesCount = data.length;
    const entiresLength = data[0].length;

    let gamma = 0;
    let epsilon = 0;

    for (let position = 0; position < entiresLength; position++) {
      const high = this.findMostCommonBit(data, position);
      gamma = (gamma << 1) | high;
      epsilon = (epsilon << 1) | !high;

      console.log(gamma.toString(2), epsilon.toString(2));
    }

    return gamma * epsilon;
  }

  async solveGold(input) {
    const data = this.parseInput(input);
    const entiresLength = data[0].length;

    let lifeSupport = 0;
    let oxygenGenerator = 0;

    let lifeSupportEntries = [...data];
    let oxygenGeneratorEntries = [...data];

    for (let position = 0; position < entiresLength; position++) {
      if (lifeSupportEntries.length > 1) {
        lifeSupportEntries = lifeSupportEntries.filter(
          filter(position, this.findMostCommonBit(lifeSupportEntries, position))
        );
      }

      if (oxygenGeneratorEntries.length > 1) {
        oxygenGeneratorEntries = oxygenGeneratorEntries.filter(
          filter(
            position,
            1 - this.findMostCommonBit(oxygenGeneratorEntries, position)
          )
        );
      }
    }

    console.log(
      lifeSupportEntries[0].join(""),
      oxygenGeneratorEntries[0].join("")
    );

    console.log(
      parseInt(lifeSupportEntries[0].join(""), 2),
      parseInt(oxygenGeneratorEntries[0].join(""), 2)
    );

    return (
      parseInt(lifeSupportEntries[0].join(""), 2) *
      parseInt(oxygenGeneratorEntries[0].join(""), 2)
    );
  }
}
