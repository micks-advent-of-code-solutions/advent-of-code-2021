import Solution from "./Solution.js";

export default class Day19 extends Solution {
  parseInput(input) {
    return input.split("\n\n").map((scanner) =>
      scanner
        .split("\n")
        .slice(1)
        .map((line) => line.split(",").map((n) => parseInt(n)))
    );
  }

  buildConnections(scannerData) {
    return scannerData.map((scanner) => {
      console.log(scanner.length);
      const connections = new Map();
      for (let a = 0; a < scanner.length; a++) {
        for (let b = 0; b < scanner.length; b++) {
          if (a !== b) {
            const distance = scanner[a].map((val, i) =>
              Math.abs(val - scanner[b][i])
            );
            distance.sort((a, b) => b - a);

            connections.set(distance.join("|"), [a, b]);
          }
        }
      }
      return connections;
    });
  }

  async solveSilver(input) {
    const scanners = this.parseInput(input);
    const connections = this.buildConnections(scanners);
    return connections;
  }

  async solveGold(input) {
    return "Not solved Yet!";
  }
}
