import Solution from "./Solution.js";

export default class Day12 extends Solution {
  parseInput(input) {
    this.nodes = {};
    input.split("\n").map((line) => this.addConnection(...line.split("-")));
  }

  addConnection(a, b) {
    if (!this.nodes[a]) {
      this.nodes[a] = {
        name: a,
        connections: [],
        type: a.toLowerCase() === a ? "small" : "big",
      };
    }

    if (!this.nodes[b]) {
      this.nodes[b] = {
        name: b,
        connections: [],
        type: b.toLowerCase() === b ? "small" : "big",
      };
    }

    this.nodes[a].connections.push(this.nodes[b]);
    this.nodes[b].connections.push(this.nodes[a]);
  }

  traverse(current, visited = [], alreadyVisitedTwice = false) {
    //console.log(current.name, visited);
    if (current.name === "end") {
      visited.push("end");
      this.routes.push(visited);
      return;
    }

    visited.push(current.name);

    for (let connection of current.connections) {
      if (connection.name != "start") {
        if (connection.type === "big" || !visited.includes(connection.name)) {
          // big Cave or first visit of small cave
          this.traverse(connection, [...visited], alreadyVisitedTwice);
        } else if (!alreadyVisitedTwice) {
          // not yet visited a node twice, doing that now
          this.traverse(connection, [...visited], true);
        }
      }
    }
  }

  async solveSilver(input) {
    this.routes = [];
    this.parseInput(input);
    this.traverse(this.nodes.start, [], true);
    return this.routes.length;
  }

  async solveGold(input) {
    this.routes = [];
    this.parseInput(input);
    this.traverse(this.nodes.start);
    return this.routes.length;
  }
}
