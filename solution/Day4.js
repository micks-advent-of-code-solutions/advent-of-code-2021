import Solution from "./Solution.js";

export default class Day4 extends Solution {
  parseInput(input) {
    const blocks = input.split("\n\n");
    return {
      numbers: blocks
        .shift()
        .split(",")
        .map((n) => parseInt(n)),
      boards: blocks.map((board) =>
        board.split("\n").map((row) =>
          row
            .split(" ")
            .filter((entry) => entry !== "")
            .map((entry) => parseInt(entry.trim()))
        )
      ),
    };
  }

  boardToProgressBoard(board, numbers) {
    return board.map((row) => row.map((entry) => numbers.indexOf(entry)));
  }

  findBingos(board, numbers) {
    const progressBoard = this.boardToProgressBoard(board, numbers);

    const bingoTimes = [];

    // 5 rows
    for (let y = 0; y < 5; y++) {
      bingoTimes.push(Math.max(...progressBoard[y]));
    }

    // 5 colums
    for (let x = 0; x < 5; x++) {
      bingoTimes.push(
        Math.max(
          ...Array(5)
            .fill()
            .map((_, i) => progressBoard[i][x])
        )
      );
    }

    return bingoTimes.sort((a, b) => b - a).pop();
  }

  async solveSilver(input) {
    const { numbers, boards } = this.parseInput(input);

    const [winnerBoardId, winnerTime] = Object.entries(
      boards.map((board) => this.findBingos(board, numbers))
    )
      .sort((a, b) => b[1] - a[1])
      .pop();

    const winnerBoard = boards[winnerBoardId];
    const winnerNumber = numbers[winnerTime];
    const winnerNumbers = numbers.slice(0, winnerTime + 1);
    const unmarkedNumbersSum = winnerBoard
      .flat()
      .filter((n) => !winnerNumbers.includes(n))
      .reduce((a, b) => a + b);

    return unmarkedNumbersSum * winnerNumber;
  }

  async solveGold(input) {
    const { numbers, boards } = this.parseInput(input);

    const [winnerBoardId, winnerTime] = Object.entries(
      boards.map((board) => this.findBingos(board, numbers))
    )
      .sort((a, b) => b[1] - a[1])
      .shift(); // only line changed from silver :)

    const winnerBoard = boards[winnerBoardId];
    const winnerNumber = numbers[winnerTime];
    const winnerNumbers = numbers.slice(0, winnerTime + 1);
    const unmarkedNumbersSum = winnerBoard
      .flat()
      .filter((n) => !winnerNumbers.includes(n))
      .reduce((a, b) => a + b);

    return unmarkedNumbersSum * winnerNumber;
  }
}
