import Solution from "./Solution.js";

const toSfn = (snailfishNumber) => {
  const runner = (sfn, depth = 0) =>
    Array.isArray(sfn)
      ? sfn.map((n) => runner(n, depth + 1))
      : { value: sfn, depth };

  return runner(snailfishNumber).flat(Number.MAX_SAFE_INTEGER);
};

const sfnMagnitude = (sfn) => {
  while (sfn.length > 1) {
    for (let i = 0; i < sfn.length - 1; i++) {
      const left = sfn[i],
        right = sfn[i + 1];

      if (left.depth === right.depth) {
        sfn.splice(i, 2, {
          value: left.value * 3 + right.value * 2,
          depth: left.depth - 1,
        });
        break;
      }
    }
  }
  return sfn[0].value;
};

const sfnAdd = (a, b) => {
  return reduceSfn(
    a.concat(b).map(({ value, depth }) => ({ value, depth: depth + 1 }))
  );
};

const reduceSfn = (sfn) => {
  const reduce = (sfn) => {
    // first explode
    for (let i = 0; i < sfn.length; i++) {
      const entry = sfn[i];
      if (entry.depth === 5) {
        if (i - 1 >= 0) {
          sfn[i - 1].value += entry.value;
        }
        if (i + 2 < sfn.length) {
          sfn[i + 2].value += sfn[i + 1].value;
        }
        sfn.splice(i, 2, { value: 0, depth: 4 });
        return true;
      }
    }
    // then split
    for (let i = 0; i < sfn.length; i++) {
      const entry = sfn[i];
      if (entry.value > 9) {
        sfn.splice(
          i,
          1,
          { value: Math.floor(entry.value / 2), depth: entry.depth + 1 },
          { value: Math.ceil(entry.value / 2), depth: entry.depth + 1 }
        );
        return true;
      }
    }
    return false;
  };

  while (reduce(sfn)) {}
  return sfn;
};

export default class Day18 extends Solution {
  parseInput(input) {
    return input.split("\n").map((line) => JSON.parse(line));
  }

  async solveSilver(input) {
    const numbers = this.parseInput(input);

    return sfnMagnitude(numbers.map((n) => toSfn(n)).reduce(sfnAdd));
  }

  async solveGold(input) {
    const numbers = this.parseInput(input);

    const results = [];

    for (let a = 0; a < numbers.length; a++) {
      for (let b = 0; b < numbers.length; b++) {
        if (a !== b) {
          const result = sfnAdd(toSfn(numbers[a]), toSfn(numbers[b]));
          results.push(sfnMagnitude(result));
        }
      }
    }

    return Math.max(...results);
  }
}
