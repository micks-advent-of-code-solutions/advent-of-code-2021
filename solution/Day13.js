import Solution from "./Solution.js";

function id([x, y]) {
  return x + "|" + y;
}

function fromId(id) {
  return id.split("|").map((x) => parseInt(x));
}

export default class Day13 extends Solution {
  parseInput(input) {
    const [dotsBlock, foldInstructionsBlock] = input
      .split("\n\n")
      .map((block) => block.split("\n"));

    this.dots = dotsBlock.map((line) =>
      line.split(",").map((coord) => parseInt(coord))
    );

    const foldingRex = /fold along (x|y)=(\d+)/;
    this.foldingInstructions = foldInstructionsBlock.map((line) => {
      const [_, axis, position] = foldingRex.exec(line);
      return { axis, position: parseInt(position) };
    });
  }

  renderDots() {
    const [maxX, maxY] = this.dots.reduce((a, b) => [
      Math.max(a[0], b[0]),
      Math.max(a[1], b[1]),
    ]);

    const line = Array(maxX + 1).fill(" ");
    const grid = Array(maxY + 1)
      .fill()
      .map((l) => [...line]);

    for (let [x, y] of this.dots) {
      grid[y][x] = "█";
    }

    return grid.map((l) => l.join("")).join("\n");
  }

  fold({ axis, position }) {
    this.dots = this.dots.map((dot) => {
      if (axis === "x" && dot[0] > position) {
        dot[0] = position - (dot[0] - position);
      } else if (axis === "y" && dot[1] > position) {
        dot[1] = position - (dot[1] - position);
      }
      return dot;
    });
  }

  async solveSilver(input) {
    this.parseInput(input);

    this.fold(this.foldingInstructions[0]);

    return new Set(this.dots.map(id)).size;
  }

  async solveGold(input) {
    this.parseInput(input);

    this.foldingInstructions.forEach((instruction) => this.fold(instruction));

    return "Just read it:\n" + this.renderDots();
  }
}
