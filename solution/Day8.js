import Solution from "./Solution.js";

const length = (length) => (input) => input.length === length;

const contains = (str) => (input) =>
  str.split("").every((char) => input.includes(char));

const containedIn = (str) => (input) =>
  input.split("").every((char) => str.includes(char));

const and =
  (...tests) =>
  (input) =>
    tests.every((test) => test(input));

const notEqualTo = (toCheck) => (input) => input !== toCheck;

const normalize = (input) => {
  const chars = input.split("");
  chars.sort();
  return chars.join("");
};

export default class Day8 extends Solution {
  parseInput(input) {
    return input
      .split("\n")
      .map((line) => line.split(" | ").map((parts) => parts.split(" ")))
      .map(([patterns, numbers]) => ({ patterns, numbers }));
  }

  getMapping(patterns) {
    const mapping = {
      1: patterns.find(length(2)),
      4: patterns.find(length(4)),
      7: patterns.find(length(3)),
      8: patterns.find(length(7)),
    };

    // 9 = 6 segments and contains 4
    mapping[9] = patterns.find(and(contains(mapping[4]), length(6)));

    // 0 = 6 segments and contains 1 and isn't 9
    mapping[0] = patterns.find(
      and(length(6), contains(mapping[1]), notEqualTo(mapping[9]))
    );

    // 6  = 6 segments and neither 9 nor 0
    mapping[6] = patterns.find(
      and(length(6), notEqualTo(mapping[9]), notEqualTo(mapping[0]))
    );

    // 5 = 5 segments and is contained in 6
    mapping[5] = patterns.find(and(length(5), containedIn(mapping[6])));

    // 3 = 5 segments and contains 1
    mapping[3] = patterns.find(and(length(5), contains(mapping[1])));

    // 2 = 5 segments and neither 5 nor 3
    mapping[2] = patterns.find(
      and(length(5), notEqualTo(mapping[5]), notEqualTo(mapping[3]))
    );

    return Object.fromEntries(
      Object.entries(mapping).map(([number, pattern]) => [
        normalize(pattern),
        number,
      ])
    );
  }

  async solveSilver(input) {
    const numbers = this.parseInput(input)
      .map((entry) => entry.numbers)
      .flat();

    return numbers
      .map((n) => n.length)
      .filter((n) => n === 2 || n === 4 || n === 3 || n === 7).length;
  }

  async solveGold(input) {
    const entries = this.parseInput(input);

    return entries
      .map((entry) => {
        const mapping = this.getMapping(entry.patterns);
        return parseInt(
          entry.numbers.map((number) => mapping[normalize(number)]).join("")
        );
      })
      .reduce((a, b) => a + b);
  }
}
