import Solution from "./Solution.js";

function normalize(n) {
  if (n === 0) return 0;
  return n / Math.abs(n);
}

export default class Day5 extends Solution {
  parseInput(input) {
    return input
      .split("\n")
      .map((line) =>
        line
          .split(" -> ")
          .map((entry) => entry.split(",").map((n) => parseInt(n)))
      );
  }

  getLineCoordinates([start, end]) {
    const coordinates = [];
    if (start[0] === end[0]) {
      // same X
      const minY = Math.min(start[1], end[1]);
      const maxY = Math.max(start[1], end[1]);
      for (let y = minY; y <= maxY; y++) {
        coordinates.push([start[0], y]);
      }
    } else if (start[1] === end[1]) {
      // same y
      const minX = Math.min(start[0], end[0]);
      const maxX = Math.max(start[0], end[0]);
      for (let x = minX; x <= maxX; x++) {
        coordinates.push([x, start[1]]);
      }
    }
    return coordinates;
  }

  getLineCoordinatesWithDiagonals([start, end]) {
    const coordinates = [];

    const steps = Math.max(
      Math.abs(start[0] - end[0]),
      Math.abs(start[1] - end[1])
    );
    const xDirection = normalize(end[0] - start[0]);
    const yDirection = normalize(end[1] - start[1]);

    for (let step = 0; step <= steps; step++) {
      coordinates.push([
        start[0] + xDirection * step,
        start[1] + yDirection * step,
      ]);
    }

    return coordinates;
  }

  async solveSilver(input) {
    const lines = this.parseInput(input);
    const grid = {};
    lines
      .flatMap((line) => this.getLineCoordinates(line))
      .forEach((coord) => {
        const id = coord.join(",");
        if (grid[id]) {
          grid[id]++;
        } else {
          grid[id] = 1;
        }
      });
    return Object.values(grid).filter((c) => c > 1).length;
  }

  async solveGold(input) {
    const lines = this.parseInput(input);
    const grid = {};
    lines
      .flatMap((line) => this.getLineCoordinatesWithDiagonals(line))
      .forEach((coord) => {
        const id = coord.join(",");
        if (grid[id]) {
          grid[id]++;
        } else {
          grid[id] = 1;
        }
      });
    return Object.values(grid).filter((c) => c > 1).length;
  }
}
