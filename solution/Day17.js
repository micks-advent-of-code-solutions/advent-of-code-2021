import Solution from "./Solution.js";

const between = (value, [from, to]) => value >= from && value <= to;

export default class Day17 extends Solution {
  parseInput(input) {
    const inputRex =
      /^target area: x=(?<x0>[\d-]+)\.\.(?<x1>[\d-]+), y=(?<y0>[\d-]+)\.\.(?<y1>[\d-]+)$/;
    const inputParts = inputRex.exec(input).groups;
    this.area = {
      x: [parseInt(inputParts.x0), parseInt(inputParts.x1)],
      y: [parseInt(inputParts.y0), parseInt(inputParts.y1)],
    };
  }

  pointInArea([x, y]) {
    return between(x, this.area.x) && between(y, this.area.y);
  }

  checkHit(startTrajectory) {
    let trajectory = [...startTrajectory];
    let position = [0, 0];
    let path = [[0, 0]];

    for (let i = 0; i < 5000; i++) {
      position = position.map((value, i) => value + trajectory[i]);

      trajectory[0] === 0
        ? 0
        : trajectory[0] > 0
        ? trajectory[0]--
        : trajectory[0]++;
      trajectory[1]--;

      path.push(position);

      if (this.pointInArea(position)) {
        return path;
      }
      // doesn't move in x and isn't over area
      if (trajectory[0] === 0 && !between(position[0], this.area.x)) {
        return false;
      }
    }
    return false;
  }

  async solveSilver(input) {
    this.parseInput(input);
    // we don't care about y, so let's change the area x
    this.area.x = [3, 3];

    const trajectories = Array(250)
      .fill()
      .map((_, i) => this.checkHit([2, i + 1]));

    return Math.max(
      ...trajectories
        .filter((t) => t)
        .map((path) => Math.max(...path.map((coord) => coord[1])))
    );
  }

  async solveGold(input) {
    this.parseInput(input);

    const trajectories = Array(200)
      .fill()
      .flatMap((_, x) =>
        Array(1000)
          .fill()
          .map((_, y) => ({
            trajectory: [x, y - 500],
            hit: this.checkHit([x, y - 500]),
          }))
      );

    return trajectories.filter((t) => t.hit).map((t) => t.trajectory).length;
  }
}
