import Solution from "./Solution.js";

const coordsToId = (...coords) => coords.join("|");
const idToCoords = (id) => id.split("|").map((c) => parseInt(c));

const getPixels = (grid, x, y) => {
  const pixels = [];

  for (let scanY = y - 1; scanY <= y + 1; scanY++) {
    for (let scanX = x - 1; scanX <= x + 1; scanX++) {
      pixels.push(coordsToId(scanX, scanY));
    }
  }

  return pixels;
};

const visualize = (grid) => {
  let minX = 0,
    maxX = 0,
    minY = 0,
    maxY = 0;
  for (let id of grid.values()) {
    const [x, y] = idToCoords(id);
    minX = Math.min(minX, x);
    maxX = Math.max(maxX, x);
    minY = Math.min(minY, y);
    maxY = Math.max(maxY, y);
  }
  console.log(minX, maxX, minY, maxY);
  const viz = [];
  for (let y = minY; y <= maxY; y++) {
    const row = [];
    for (let x = minX; x <= maxX; x++) {
      row.push(grid.has(coordsToId(x, y)) ? "#" : ".");
    }
    viz.push(row.join(""));
  }
  console.log(viz.join("\n"));
};

const calculateNextImage = (image, mapping) => {
  const mappedImage = new Set();
  // const coordsToCheck = new Set();

  // for (let id of image.values()) {
  //   getPixels(image, ...idToCoords(id)).forEach((id) => coordsToCheck.add(id));
  // }
  // visualize(coordsToCheck);

  // for (let id of coordsToCheck.values()) {
  //   const mappingIndex = parseInt(
  //     getPixels(image, ...idToCoords(id))
  //       .map((id) => (image.has(id) ? 1 : 0))
  //       .join(""),
  //     2
  //   );
  //   if (mapping[mappingIndex] === 1) {
  //     mappedImage.add(id);
  //   }
  // }

  let minX = 0,
    maxX = 0,
    minY = 0,
    maxY = 0;
  for (let id of image.values()) {
    const [x, y] = idToCoords(id);
    minX = Math.min(minX, x - 10);
    maxX = Math.max(maxX, x + 10);
    minY = Math.min(minY, y - 10);
    maxY = Math.max(maxY, y + 10);
  }
  console.log(minX, maxX, minY, maxY);
  for (let y = minY; y <= maxY; y++) {
    for (let x = minX; x <= maxX; x++) {
      const mappingIndex = parseInt(
        getPixels(image, x, y)
          .map((id) => (image.has(id) ? 1 : 0))
          .join(""),
        2
      );
      if (mapping[mappingIndex] === 1) {
        mappedImage.add(coordsToId(x, y));
      }
    }
  }

  return mappedImage;
};

export default class Day20 extends Solution {
  parseInput(input) {
    const [mappingBlock, imageBlock] = input.split("\n\n");

    const mapping = mappingBlock
      .split("\n")
      .join("")
      .split("")
      .map((char) => (char === "#" ? 1 : 0));

    const image = new Set();
    imageBlock.split("\n").forEach((row, y) =>
      row.split("").forEach((char, x) => {
        if (char === "#") {
          image.add(coordsToId(x, y));
        }
      })
    );

    const initialSize = imageBlock.split("\n").length;

    return { mapping, image, initialSize };
  }

  async solveSilver(input) {
    let { mapping, image } = this.parseInput(input);

    visualize(image);
    for (let i = 0; i < 2; i++) {
      image = calculateNextImage(image, mapping);
    }
    return [...image.values()].filter((id) => {
      const [x, y] = idToCoords(id);
      return x >= -5 && x <= 105 && y >= -5 && y <= 105;
    }).length;
  }

  async solveGold(input) {
    let { mapping, image, initialSize } = this.parseInput(input);

    visualize(image);
    for (let i = 0; i < 5; i++) {
      image = calculateNextImage(image, mapping);
      if (i % 2 === 1) {
        console.log("stip down");
        visualize(image);
        image = new Set(
          [...image.values()].filter((id) => {
            const [x, y] = idToCoords(id);
            return (
              x >= -i - 1 &&
              x <= initialSize + i + 1 &&
              y >= -i - 1 &&
              y <= initialSize + i + 1
            );
          })
        );
      }
      visualize(image);
    }

    return image.size;
  }
}
