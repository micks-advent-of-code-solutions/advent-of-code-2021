import Solution from "./Solution.js";

// aProg(n) = 1+2+3+4+5+...+n, see https://en.wikipedia.org/wiki/Arithmetic_progression
function aProg(n) {
  return (n * (n + 1)) / 2;
}

export default class Day7 extends Solution {
  async solveSilver(input) {
    const crabs = input.split(",").map((c) => parseInt(c));
    const maxCrab = Math.max(...crabs);
    const effortPerPosition = Array(maxCrab);
    for (let pos = 0; pos < maxCrab; pos++) {
      let effort = 0;
      for (let crab of crabs) {
        effort += Math.abs(pos - crab);
      }
      effortPerPosition[pos] = effort;
    }
    return effortPerPosition.sort((a, b) => a - b).shift();
  }

  async solveGold(input) {
    const crabs = input.split(",").map((c) => parseInt(c));
    const maxCrab = Math.max(...crabs);
    const effortPerPosition = Array(maxCrab);
    for (let pos = 0; pos < maxCrab; pos++) {
      let effort = 0;
      for (let crab of crabs) {
        effort += aProg(Math.abs(pos - crab));
      }
      effortPerPosition[pos] = effort;
    }
    return effortPerPosition.sort((a, b) => a - b).shift();
  }
}
