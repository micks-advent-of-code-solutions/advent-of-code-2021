import Solution from "./Solution.js";

class DeterministicDice {
  #state = -1;

  roll() {
    this.#state++;
    return (this.#state % 100) + 1;
  }

  roll3AndCombine() {
    return this.roll() + this.roll() + this.roll();
  }

  get rollCount() {
    return this.#state + 1;
  }
}

export default class Day21 extends Solution {
  parseInput(input) {
    return input.split("\n").map((line) => ({
      position: parseInt(line.split("position: ")[1]) - 1,
      score: 0,
    }));
  }
  async solveSilver(input) {
    const endGameScore = 1000;
    const players = this.parseInput(input);
    const dice = new DeterministicDice();
    while (
      !(players[0].score >= endGameScore || players[1].score >= endGameScore)
    ) {
      players.every((player) => {
        player.position = (player.position + dice.roll3AndCombine()) % 10;
        player.score += player.position + 1;
        if (player.score >= endGameScore) {
          return false;
        }
        return true;
      });
    }
    if (players[0].score >= endGameScore) {
      return players[1].score * dice.rollCount;
    } else {
      return players[0].score * dice.rollCount;
    }
  }

  async solveGold(input) {
    const wins = [0, 0];
    const gameQueue = [this.parseInput(input)];
    debugger;
    return "Not solved Yet!";
  }
}
