import Solution from "./Solution.js";

export default class Day6 extends Solution {
  tick(fish) {
    fish--;
    if (fish === -1) {
      return [6, 8];
    }
    return fish;
  }
  async solveSilver(input) {
    //
    let fishes = input.split(",").map((f) => parseInt(f));
    for (let day = 0; day < 80; day++) {
      fishes = fishes.flatMap(this.tick);
    }
    return fishes.length;
  }

  async solveGold(input) {
    const fishes = input.split(",").map((f) => parseInt(f));
    const fishesByAge = Array(9).fill(0);
    fishes.forEach((fish) => fishesByAge[fish]++);
    for (let day = 0; day < 256; day++) {
      const currentFishes = fishesByAge.shift();
      fishesByAge[6] += currentFishes;
      fishesByAge.push(currentFishes);
    }
    return fishesByAge.reduce((a, b) => a + b);
  }
}
