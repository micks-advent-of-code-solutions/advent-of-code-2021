import Solution from "./Solution.js";

function coordToId(...coords) {
  return coords.join(",");
}

function idToCoord(id) {
  return id.split(",").map((c) => parseInt(c));
}

class Cube {
  constructor(bounds) {
    this.bounds = bounds;
  }

  constraintInto(cube) {
    const hasOverlap = this.bounds.every(([from, to], axis) => {
      const [constrainerFrom, constrainerTo] = cube.bounds[axis];

      if (constrainerFrom > to || constrainerTo < from) {
        // AKA no overlap
        return false;
      }

      const constainedBound = [
        Math.max(from, constrainerFrom),
        Math.min(to, constrainerTo),
      ];

      this.bounds[axis] = constainedBound;

      return true;
    });

    if (!hasOverlap) {
      this.bounds = null;
    }
  }

  getCordIds() {
    if (this.bounds) {
      const [[x1, x2], [y1, y2], [z1, z2]] = this.bounds;
      const ids = new Set();
      for (let x = x1; x <= x2; x++) {
        for (let y = y1; y <= y2; y++) {
          for (let z = z1; z <= z2; z++) {
            ids.add(coordToId(x, y, z));
          }
        }
      }
      return ids;
    }
    return new Set();
  }

  toString() {
    return `Cube${JSON.stringify(this.bounds)}`;
  }
}

export default class Day22 extends Solution {
  parseInput(input, constaint = null) {
    return input.split("\n").map((line) => {
      const [state, cubeData] = line.split(" ");

      const cube = new Cube(
        cubeData.split(",").map((axis) =>
          axis
            .split("=")[1]
            .split("..")
            .map((num) => parseInt(num))
        )
      );

      if (constaint) {
        cube.constraintInto(constaint);
      }

      return {
        state: state === "on",
        cube,
      };
    });
  }

  async solveSilver(input) {
    const area = new Cube([
      [-50, 50],
      [-50, 50],
      [-50, 50],
    ]);
    const volumeState = new Set();
    const instructions = this.parseInput(input, area);

    console.log(instructions);

    for (let { state, cube } of instructions) {
      const ids = cube.getCordIds();
      if (state) {
        for (let id of ids) {
          volumeState.add(id);
        }
      } else {
        for (let id of ids) {
          volumeState.delete(id);
        }
      }
    }

    return volumeState.size;
  }

  async solveGold(input) {
    // This breaks horribly :D
    return;
    const volumeState = new Set();
    const instructions = this.parseInput(input);

    for (let { state, cube } of instructions) {
      const ids = cube.getCordIds();
      if (state) {
        for (let id of ids) {
          volumeState.add(id);
        }
      } else {
        for (let id of ids) {
          volumeState.delete(id);
        }
      }
    }

    return volumeState.size;
  }
}
