import Solution from "./Solution.js";

const openBrackets = new Set("({<[".split(""));

const closedToOpenBracket = Object.fromEntries(
  "(),{},<>,[]".split(",").map((pair) => pair.split(""))
);

const syntaxScoring = {
  ")": 3,
  "]": 57,
  "}": 1197,
  ">": 25137,
};

const autoCompleteScoring = Object.fromEntries(
  "([{<".split("").map((char, i) => [char, i + 1])
);

export default class Day10 extends Solution {
  parseInput(input) {
    return input.split("\n");
  }

  checkSyntax(line) {
    const stack = [];

    for (let char of line) {
      if (openBrackets.has(char)) {
        stack.push(char);
      } else {
        const lastEntry = stack.pop();
        if (char !== closedToOpenBracket[lastEntry]) {
          return { error: "Syntax", lastChar: char };
        }
      }
    }
    if (stack.length > 0) {
      return { error: "Completion", stack };
    }
  }

  async solveSilver(input) {
    const lines = this.parseInput(input);

    return lines
      .map(this.checkSyntax)
      .filter((entry) => entry.error === "Syntax")
      .map((entry) => entry.lastChar)
      .reduce((a, b) => a + syntaxScoring[b], 0);
  }

  async solveGold(input) {
    const lines = this.parseInput(input);
    const scores = lines
      .map(this.checkSyntax)
      .filter((entry) => entry.error === "Completion")
      .map((entry) =>
        entry.stack.reduceRight((a, b) => a * 5 + autoCompleteScoring[b], 0)
      );
    scores.sort((a, b) => b - a);
    return scores[Math.floor(scores.length / 2)];
  }
}
