import Solution from "./Solution.js";

// Precalculate surrunding grid positions
const surroundingCoordDelta = [];
for (let x = -1; x < 2; x++) {
  for (let y = -1; y < 2; y++) {
    if (!(x === 0 && y === 0)) {
      surroundingCoordDelta.push([x, y]);
    }
  }
}

export default class Day11 extends Solution {
  parseInput(input) {
    this.grid = input
      .split("\n")
      .map((row) => row.split("").map((cell) => parseInt(cell)));

    this.width = this.grid[0].length;
    this.height = this.grid.length;
  }

  id([x, y]) {
    return x + "|" + y;
  }

  fromId(id) {
    return id.split("|").map((x) => parseInt(x));
  }

  getCell(x, y) {
    return this.grid[y][x];
  }

  getSurroundingCellCoords([x, y]) {
    return surroundingCoordDelta
      .map(([xDiff, yDiff]) => [x + xDiff, y + yDiff])
      .filter(
        ([x, y]) => x > -1 && y > -1 && x < this.width && y < this.height
      );
  }

  calculateStep() {
    // energyIncrease + finding initial pings
    const alreadyPinged = new Set();
    const toPing = [];
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        const level = this.grid[y][x] + 1;
        this.grid[y][x] = level;
        if (level > 9) {
          toPing.push([x, y]);
        }
      }
    }
    // pinging galore
    while (toPing.length > 0) {
      const pinger = toPing.shift();
      if (alreadyPinged.has(this.id(pinger))) continue;
      const surrounding = this.getSurroundingCellCoords(pinger);

      for (let octo of surrounding) {
        const [x, y] = octo;
        const level = this.grid[y][x] + 1;
        if (level > 9) {
          toPing.push(octo);
        }
        this.grid[y][x] = level;
      }

      alreadyPinged.add(this.id(pinger));
    }

    // set pinged to zero
    for (let pinged of alreadyPinged) {
      const [x, y] = this.fromId(pinged);
      this.grid[y][x] = 0;
    }

    return alreadyPinged.size;
  }

  printGrid() {
    console.log(
      this.grid
        .map((line) =>
          line.map((point) => point.toString().padStart(2, " ")).join(" ")
        )
        .join("\n") + "\n"
    );
  }

  async solveSilver(input) {
    this.parseInput(input);
    let pings = 0;
    for (let i = 0; i < 100; i++) {
      pings += this.calculateStep();
    }
    return pings;
  }

  async solveGold(input) {
    this.parseInput(input);
    const requiredPings = this.width * this.height;
    let step = 1;
    while (this.calculateStep() != requiredPings) {
      step++;
    }
    return step;
  }
}
