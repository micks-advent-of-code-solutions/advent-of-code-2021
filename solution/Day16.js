import Solution from "./Solution.js";

const hexBinaryMapping = Object.fromEntries(
  Array(16)
    .fill()
    .map((_, i) => [
      i.toString(16).toUpperCase(),
      i.toString(2).padStart(4, "0"),
    ])
);

const idMeaning = [
  "sum",
  "product",
  "minimum",
  "maximum",
  "literal",
  "greater than",
  "less than",
  "equal to",
];

const resolve = {
  sum: (entries) => entries.reduce((a, b) => a + b),
  product: (entries) => entries.reduce((a, b) => a * b),
  minimum: (entries) => Math.min(...entries),
  maximum: (entries) => Math.max(...entries),
  "greater than": ([a, b]) => (a > b ? 1 : 0),
  "less than": ([a, b]) => (a < b ? 1 : 0),
  "equal to": ([a, b]) => (a === b ? 1 : 0),
};

export default class Day16 extends Solution {
  parseInput(input) {
    const binary = input
      .split("")
      .map((char) => hexBinaryMapping[char])
      .join("");

    return this.parsePackage(binary);
  }

  parsePackage(data) {
    const pkg = {
      version: parseInt(data.substring(0, 3), 2),
      id: parseInt(data.substring(3, 6), 2),
    };

    pkg.meaning = idMeaning[pkg.id];

    switch (pkg.id) {
      case 4:
        //console.log("Literal");
        let position = 6;
        let literalString = "";
        let readOn = true;
        while (readOn) {
          readOn = data[position] === "1";
          literalString += data.substring(position + 1, position + 5);
          position += 5;
        }
        pkg.length = position;
        pkg.literal = parseInt(literalString, 2);
        break;

      default:
        //console.log("operator");
        pkg.lengthType = parseInt(data[6]);
        pkg.packages = [];
        if (pkg.lengthType === 0) {
          const subPackageLength = parseInt(data.substring(7, 7 + 15), 2);
          pkg.subPackageLength = subPackageLength;

          const subPackages = data.substring(7 + 15, 7 + 15 + subPackageLength);

          let position = 0;
          while (position < subPackageLength) {
            let subPkg = this.parsePackage(
              subPackages.substring(position, subPackageLength)
            );
            if (subPkg.length === undefined) {
              debugger;
            }
            position += subPkg.length;
            pkg.packages.push(subPkg);
          }

          pkg.length = 7 + 15 + subPackageLength;
        } else {
          const packageCount = parseInt(data.substring(7, 7 + 11), 2);

          const subPackages = data.substring(7 + 11);
          let position = 0;

          for (let i = 0; i < packageCount; i++) {
            let subPkg = this.parsePackage(subPackages.substring(position));
            position += subPkg.length;
            pkg.packages.push(subPkg);
          }
          pkg.length = 7 + 11 + position;
        }
        pkg.subPackageCount = pkg.packages.length;
        break;
    }

    return pkg;
  }

  calculateVersionSum(pkg) {
    let versionSum = pkg.version;
    if (pkg.packages) {
      versionSum += pkg.packages
        .map((subPkg) => this.calculateVersionSum(subPkg))
        .reduce((a, b) => a + b);
    }

    return versionSum;
  }

  calculate(pkg) {
    if (pkg.literal) {
      return pkg.literal;
    } else {
      const values = pkg.packages.map((subPkg) => this.calculate(subPkg));
      return resolve[idMeaning[pkg.id]](values);
    }
  }

  async solveSilver(input) {
    const pkg = this.parseInput(input);
    return this.calculateVersionSum(pkg);
  }

  async solveGold(input) {
    const pkg = this.parseInput(input);
    return this.calculate(pkg);
  }
}
