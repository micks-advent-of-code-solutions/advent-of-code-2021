import Solution from "./Solution.js";

function sortNodes(a, b) {
  if (a.distance === b.distance) {
    // If same distance, sort by distance from center line :)
    return Math.abs(a.x - a.y) - Math.abs(b.x - b.y);
  } else {
    return a.distance - b.distance;
  }
}

export default class Day15 extends Solution {
  parseInput(input) {
    this.grid = input.split("\n").map((line, y) =>
      line.split("").map((entry, x) => ({
        level: parseInt(entry),
        x,
        y,
        distance: x + y === 0 ? 0 : Number.MAX_SAFE_INTEGER,
        solved: x + y === 0,
      }))
    );
    this.width = this.grid[0].length;
    this.height = this.grid.length;
  }

  tile() {
    this.grid = Array(this.height * 5)
      .fill()
      .map((row, y) =>
        Array(this.width * 5)
          .fill()
          .map((_, x) => {
            const original = this.grid[y % this.height][x % this.width];
            const diff =
              Math.floor(x / this.width) + Math.floor(y / this.height);
            return {
              x,
              y,
              distance: x + y === 0 ? 0 : Number.MAX_SAFE_INTEGER,
              level: ((original.level + diff - 1) % 9) + 1,
              solved: x + y === 0,
            };
          })
      );
    this.width = this.grid[0].length;
    this.height = this.grid.length;
  }

  getSurroundingNodes({ x, y }) {
    return [
      [x - 1, y],
      [x + 1, y],
      [x, y - 1],
      [x, y + 1],
    ]
      .filter(([x, y]) => x > -1 && y > -1 && x < this.width && y < this.height)
      .map(([x, y]) => this.grid[y][x]);
  }

  russianDude() {
    const nodes = [];
    const goal = this.grid[this.height - 1][this.width - 1];
    let node = this.grid[0][0];

    while (node != goal) {
      this.getSurroundingNodes(node)
        .filter((node) => !node.solved)
        .forEach((surroundingNode) => {
          // node is interesting, save in list
          if (!nodes.includes(surroundingNode)) nodes.push(surroundingNode);
          const newDistance = node.distance + surroundingNode.level;
          if (newDistance < surroundingNode.distance) {
            surroundingNode.distance = newDistance;
            surroundingNode.previous = node;
          }
        });

      nodes.sort(sortNodes);
      node.solved = true;
      node = nodes.shift();
    }

    return goal;
  }

  drawRoute(node) {
    const grid = Array(this.height)
      .fill()
      .map((_, y) =>
        Array(this.width)
          .fill()
          .map((_, x) => " " + this.grid[x][y].level + " ")
      );
    let currentNode = node;
    while (currentNode) {
      grid[currentNode.y][currentNode.x] = "[" + currentNode.level + "]";
      currentNode = currentNode.previous;
    }
    console.log(grid.map((row) => row.join("")).join("\n"));
  }

  async solveSilver(input) {
    this.parseInput(input);
    const route = this.russianDude();
    //this.drawRoute(route);
    return route.distance;
  }

  async solveGold(input) {
    this.parseInput(input);
    this.tile();
    const route = this.russianDude();
    //this.drawRoute(route);
    return route.distance;
  }
}
