import Solution from "./Solution.js";

export default class Day14 extends Solution {
  parseInput(input) {
    const [startBlock, rulesBlock] = input.split("\n\n");

    this.start = startBlock.split("");
    this.rules = Object.fromEntries(
      rulesBlock.split("\n").map((rule) => rule.split(" -> "))
    );
  }

  expand(arr, rules = this.rules) {
    for (let pos = 0; pos < arr.length - 1; pos++) {
      const mapped = rules[arr[pos] + arr[pos + 1]];
      arr.splice(pos + 1, 0, ...mapped);
      pos += mapped.length;
    }
    return arr;
  }

  getQuantities(arr) {
    const quantities = {};
    arr.forEach((entry) => {
      if (entry in quantities) {
        quantities[entry]++;
      } else {
        quantities[entry] = 1;
      }
    });
    return quantities;
  }

  // Naive approach
  async solveSilver(input) {
    this.parseInput(input);

    let expanded = this.start;
    for (let i = 0; i < 10; i++) {
      expanded = this.expand(expanded);
    }
    const quantities = this.getQuantities(expanded);

    const counts = Object.values(quantities);
    return Math.max(...counts) - Math.min(...counts);
  }

  // Naive approach was too slow, so less naive apporach for gold :)
  async solveGold(input) {
    this.parseInput(input);

    const ruleMap = Object.fromEntries(
      Object.entries(this.rules).map(([key, value]) => [
        key,
        [key[0] + value, value + key[1]],
      ])
    );

    let patternCounts = Object.fromEntries(
      Object.keys(ruleMap).map((key) => [key, 0])
    );

    const charCounts = Object.fromEntries(
      [
        ...new Set(
          Object.keys(ruleMap).flatMap((rule) => rule.split(""))
        ).keys(),
      ].map((key) => [key, 0])
    );

    for (let pos = 0; pos < this.start.length - 1; pos++) {
      patternCounts[this.start[pos] + this.start[pos + 1]]++;
    }
    this.start.forEach((char) => charCounts[char]++);

    for (let i = 0; i < 40; i++) {
      const newPatternCounts = Object.fromEntries(
        Object.keys(ruleMap).map((key) => [key, 0])
      );
      Object.entries(patternCounts).forEach(([pattern, amount]) => {
        // add charcounts
        charCounts[this.rules[pattern]] += amount;
        // add ruleCounts
        ruleMap[pattern].forEach(
          (entry) => (newPatternCounts[entry] += amount)
        );
      });
      patternCounts = newPatternCounts;
    }

    const counts = Object.values(charCounts);
    return Math.max(...counts) - Math.min(...counts);
  }
}
