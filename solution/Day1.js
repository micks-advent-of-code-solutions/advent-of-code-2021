import Solution from "./Solution.js";

export default class Day1 extends Solution {
  getIncreases(measurements) {
    let goingDeeper = 0;
    for (let i = 1; i < measurements.length; i++) {
      if (measurements[i - 1] < measurements[i]) {
        goingDeeper++;
      }
    }
    return goingDeeper;
  }

  async solveSilver(input) {
    const measurements = input.split("\n").map((l) => parseInt(l));
    return this.getIncreases(measurements);
  }

  async solveGold(input) {
    const measurements = input.split("\n").map((l) => parseInt(l));
    const slidingMeasurements = [];
    for (let i = 2; i < measurements.length; i++) {
      slidingMeasurements.push(
        measurements[i - 2] + measurements[i - 1] + measurements[i]
      );
    }
    return this.getIncreases(slidingMeasurements);
  }
}
