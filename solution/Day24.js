import Solution from "./Solution.js";

class Alu {
  registerNames = new Set("wxyz".split(""));
  registers = {
    w: 0,
    x: 0,
    y: 0,
    z: 0,
  };

  input = [];

  inp(reg) {
    this.registers[reg] = this.input.shift();
  }
  add(reg, regVal) {
    this.registers[reg] += regVal;
  }
  mul(reg, regVal) {
    this.registers[reg] *= regVal;
  }
  div(reg, regVal) {
    this.registers[reg] = Math.floor(this.registers[reg] / regVal);
  }
  mod(reg, regVal) {
    this.registers[reg] %= regVal;
  }
  eql(reg, regVal) {
    this.registers[reg] = (this.registers[reg] === regVal) * 1;
  }

  reset() {
    this.registers = {
      w: 0,
      x: 0,
      y: 0,
      z: 0,
    };
  }

  loadProgram(program) {
    this.program = program.split("\n").map((line) => {
      let [instruction, paramA, paramB] = line.split(" ");
      if (paramB !== undefined && !this.registerNames.has(paramB)) {
        paramB = parseInt(paramB);
      }

      return { instruction, paramA, paramB };
    });
  }

  runProgram(input) {
    this.reset();
    this.input = input;
    for (let { instruction, paramA, paramB } of this.program) {
      if (typeof paramB === "string") {
        this[instruction](paramA, this.registers[paramB]);
      } else {
        this[instruction](paramA, paramB);
      }
    }
    return this.registers.z;
  }
}

function* ModelNumberGenerator() {
  for (let a = 1; a < 10; a++) {
    for (let b = 1; b < 10; b++) {
      for (let c = 1; c < 10; c++) {
        for (let d = 1; d < 10; d++) {
          for (let e = 1; e < 10; e++) {
            for (let f = 1; f < 10; f++) {
              for (let g = 1; g < 10; g++) {
                for (let h = 1; h < 10; h++) {
                  for (let i = 1; i < 10; i++) {
                    for (let j = 1; j < 10; j++) {
                      for (let k = 1; k < 10; k++) {
                        for (let l = 1; l < 10; l++) {
                          for (let m = 1; m < 10; m++) {
                            for (let n = 1; n < 10; n++) {
                              yield [a, b, c, d, e, f, g, h, i, j, k, l, m, n];
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

export default class Day24 extends Solution {
  async solveSilver(input) {
    const alu = new Alu();
    alu.loadProgram(input);
    const modelNumberGenerator = ModelNumberGenerator();
    let smallestOutput = Number.MAX_SAFE_INTEGER;

    const ignore = 17000000;

    console.log("Throwing away old shit");
    for (let j = 0; j < ignore; j++) {
      modelNumberGenerator.next().value;
    }
    console.log("Starting calc");
    for (let i = ignore; i < ignore + 10000001; i++) {
      if (i % 1000000 === 0) {
        console.log(i);
      }
      const output = alu.runProgram(modelNumberGenerator.next().value);
      smallestOutput = Math.min(smallestOutput, output);
      if (output === 0) {
        console.log("One before:");
        console.log(modelNumberGenerator.next().value);
        break;
      }
    }
    console.log(modelNumberGenerator.next().value);
    return smallestOutput;
  }

  async solveGold(input) {
    return "Not solved Yet!";
  }
}
