import Solution from "./Solution.js";

export default class Day9 extends Solution {
  parseInput(input) {
    this.grid = input
      .split("\n")
      .map((row) => row.split("").map((cell) => parseInt(cell)));

    this.width = this.grid[0].length;
    this.height = this.grid.length;
  }

  id([x, y]) {
    return x + "|" + y;
  }

  fromId(id) {
    return id.split("|").map((x) => parseInt(x));
  }

  floodFill([x, y]) {
    const area = new Set([this.id([x, y])]);
    const toCheck = new Set(this.getSurroundingCellCoords([x, y]).map(this.id));

    while (toCheck.size > 0) {
      const cellId = [...toCheck.values()].shift();
      toCheck.delete(cellId);
      const cellToCheck = this.fromId(cellId);

      if (this.getCell(...cellToCheck) !== 9) {
        // is in Basin
        area.add(this.id(cellToCheck));
        // add surrounding to check
        this.getSurroundingCellCoords(cellToCheck).forEach((c) => {
          const cellId = this.id(c);
          if (!area.has(cellId)) {
            toCheck.add(this.id(c));
          }
        });
      }
    }

    return area.size;
  }

  getCell(x, y, fallback = 9) {
    if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
      return fallback;
    }
    return this.grid[y][x];
  }

  getSurroundingCellCoords([x, y]) {
    return [
      [x - 1, y],
      [x + 1, y],
      [x, y - 1],
      [x, y + 1],
    ];
  }

  getSurroundingCells(x, y) {
    return [
      this.getCell(x - 1, y),
      this.getCell(x + 1, y),
      this.getCell(x, y - 1),
      this.getCell(x, y + 1),
    ];
  }

  async solveSilver(input) {
    this.parseInput(input);

    const lowPoints = [];

    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        const cell = this.getCell(x, y);
        const surrounding = this.getSurroundingCells(x, y);
        if (cell < Math.min(...surrounding)) {
          lowPoints.push(cell);
        }
      }
    }
    return lowPoints.map((height) => height + 1).reduce((a, b) => a + b);
  }

  async solveGold(input) {
    this.parseInput(input);

    const lowPoints = [];

    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        const cell = this.getCell(x, y);
        const surrounding = this.getSurroundingCells(x, y);
        if (cell < Math.min(...surrounding)) {
          lowPoints.push([x, y]);
        }
      }
    }

    const basins = lowPoints.map((point) => this.floodFill(point));
    basins.sort((a, b) => b - a);
    return basins.slice(0, 3).reduce((a, b) => a * b);
  }
}
